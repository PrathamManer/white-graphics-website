/**
 * scripts.js
 * Contains Script for basic static website named "White Graphics"
 */


$(window).on('load', function(){
    $("#preloader").delay(1000).fadeOut('slow');
    
    
    /*Team Section*/
    $("#team-members").owlCarousel({
        items: 2,
        autoplay: true,
        loop: true,
        nav: true,
        navText: ['<i class="lni lni-chevron-left-circle"></i>', '<i class="lni lni-chevron-right-circle"></i>'],
        dots: false,
        smartSpeed: 700,
        margin: 20
    });
    
    /*Work-02 Section*/
    $("#cards").owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        navText: ['<i class="lni lni-chevron-left-circle"></i>', '<i class="lni lni-chevron-right-circle"></i>'],
        dots: false,
        smartSpeed: 700,
//        margin: 20
    });
    
    /*Work-05 Section*/
    $("#clients").owlCarousel({
        items: 6,
        loop: true,
        nav: true,
        navText: ['<i class="lni lni-chevron-left-circle"></i>', '<i class="lni lni-chevron-right-circle"></i>'],
        dots: false,
        smartSpeed: 700,
//        margin: 5
    });
    
    /*Progress Bar*/
    $("#progress-elements").waypoint(function() {
        $(".progress-bar").each(function() {
            $(this).animate({
                width: $(this).attr("aria-valuenow")+"%"
            }, 800);
        });
        
        this.destroy(); //deletes the waypoint object binded with progress-elements
        // so that it only animates once.
    }, {
        offset: 'bottom-in-view'
    });
});
